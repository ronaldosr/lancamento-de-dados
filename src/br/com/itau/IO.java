package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {

    public static void imprimirMensagemInicial() {
        System.out.println("**** Lançamento de dados ****");
    }

    public static Map<String, Integer> solicitarDados() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Quantas faces o dado possui?");
        Integer qtdFaces = scanner.nextInt();

        System.out.println("");
        System.out.println("Quantos dados você deseja jogar?");
        Integer qtdSorteios = scanner.nextInt();

        System.out.println("");
        System.out.println("Quantos lancamentos consecutivos você deseja fazer?");
        Integer qtdLancamentos = scanner.nextInt();

        Map<String, Integer> parametros = new HashMap<>();
        parametros.put("qtdFaces", qtdFaces);
        parametros.put("qtdSorteios", qtdSorteios);
        parametros.put("qtdLancamentos", qtdLancamentos);

        return parametros;
    }

    public static int menuOpcoes(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("(1) - Novo Jogo");
        System.out.println("(0) - Sair");
        return scanner.nextInt();
    }

    public static double obtemValor() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
