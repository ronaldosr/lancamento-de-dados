package br.com.itau;

import java.io.DataOutput;
import java.util.Map;

public class Menu {

    public Menu(){

        int opcao = 99;

        while (opcao != 0){
            opcao = IO.menuOpcoes();

            if (opcao == 1) {
                // Nogo Jogo
                Map<String, Integer> parametros = IO.solicitarDados();
                int qtdFaces = parametros.get("qtdFaces");
                int qtdSorteio = parametros.get("qtdSorteios");
                int qtdLancamentos = parametros.get("qtdLancamentos");

                Lancamento lancamento = new Lancamento(qtdLancamentos);
                lancamento.setSorteios(lancamento.lancar(qtdSorteio));

                Impressora impressora = new Impressora();
                impressora.imprimirTela(lancamento);


            } else if (opcao == 0) {
                System.out.println("Bye....");

            } else {
                System.out.println("Opção inválida");
                System.out.println("");
            }
        }

    }
}
