package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Sorteio {

    private int qtdSorteios;
    private int somaDados;
    private List<Dado> dados;

    public Sorteio(int qtdSorteios) {
        this.qtdSorteios = qtdSorteios;
    }

    public List<Dado> sortear( int qtdSorteios) {

            if (qtdSorteios > 0) {

                //TODO
                //Refatorar para subistituir a quantidade padrão de faces em um dado
                int faces = 6;

                List<Dado> dados = new ArrayList<>();

                for (int i = 0; i < qtdSorteios; i++) {
                    Dado dado = new Dado(faces);
                    dado.jogarDado(faces);
                    dados.add(dado);
                }

                return dados;

            } else {
                System.out.println("Por favor, informe a quantidade de sorteios.");
                return null;
            }


    }

    public int somaSorteio(List<Dado> dados) {
        if (dados != null) {
            int somaDados = 0;
            for (Dado dado : dados) {
                somaDados = somaDados + dado.getValor();
            }
            return somaDados;
        }
        return 0;
    }

    public int getQtdSorteio() {
        return qtdSorteios;
    }

    public void setQtdSorteio(int qtdSorteios) {
        this.qtdSorteios = qtdSorteios;
    }

    public List<Dado> getDados() {
        return dados;
    }

    public int getSomaDados() {
        return somaDados;
    }

    public void setSomaDados(int somaDados) {
        this.somaDados = somaDados;
    }

}
