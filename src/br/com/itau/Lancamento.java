package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Lancamento {

    private int id;
    private int qtdLancamentos;
    private List<Sorteio> sorteios;

    public Lancamento(int qtdLancamentos) {
        this.qtdLancamentos = qtdLancamentos;
    }

    public List<Sorteio> lancar(int qtdSorteios) {
        if (this.qtdLancamentos > 0) {
            List<Sorteio> sorteios = new ArrayList<>();

            for (int i = 0; i < this.qtdLancamentos; i++) {
                Sorteio sorteio = new Sorteio(qtdSorteios);
                sorteio.sortear(qtdSorteios);
                sorteios.add(sorteio);
            }
            return sorteios;

        } else {
            System.out.println("Por favor, informe a quantidade de lancamentos.");
            return null;
        }
    }

    public Lancamento(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQtdLancamentos() {
        return qtdLancamentos;
    }

    public void setQtdLancamentos(int qtdLancamentos) {
        this.qtdLancamentos = qtdLancamentos;
    }

    public List<Sorteio> getSorteios() {
        return sorteios;
    }

    public void setSorteios(List<Sorteio> sorteios) {
        this.sorteios = sorteios;
    }
}
