package br.com.itau;

import java.util.Random;

public class Dado {

    private int valor;
    private int faces;

    public Dado(int faces) {
        this.faces = faces;
    }

    public int jogarDado(int faces) {
        Random random = new Random();
        return this.valor =  random.nextInt(faces);
    }

    public int getValor() {
        return valor;
    }

    public void setFaces(int faces) {
        if (faces < 6) {
            this.faces = 6;
        }
        this.faces = faces;
    }

}
