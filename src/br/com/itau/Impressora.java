package br.com.itau;

import java.util.List;

public class Impressora {

    public void imprimirTela(Lancamento lancamento) {
        if (lancamento != null) {

            if (lancamento.getSorteios() != null) {

                for (Sorteio sorteio: lancamento.getSorteios()) {

                    if (sorteio.getDados() != null) {
                        String linha = "";

                        int soma  = sorteio.somaSorteio(sorteio.getDados());

                        for (int i = 0; i < sorteio.getDados().size(); i++) {
                            Dado dado = sorteio.getDados().get(i);
                            linha = linha + dado.getValor() + " ";
                        }
                        linha = linha + soma;
                        System.out.println(linha);
                    }
                }
            } else {
                System.out.println("Não há sorteios para impressão.");
            }

        } else {
            System.out.println("Não há lancamentos para impressão.");
        }
    }
}
